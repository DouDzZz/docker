#!/bin/bash

case $TS_VERSION in
  LATEST)
    export TS_VERSION=`/get-version`
    ;;
esac

cd /data

export LD_LIBRARY_PATH=".:/data:/data/redist"

TS3ARGS=""
if [ -e /data/ts3server.ini ]; then
  TS3ARGS="inifile=/data/ts3server.ini"
else
  TS3ARGS="createinifile=1"
fi

if [ -n "$SERVERADMIN_PASSWORD" ]; then
  TS3ARGS="$TS3ARGS serveradmin_password=$SERVERADMIN_PASSWORD"
fi

if [ "accept" = "$TS3SERVER_LICENSE" ]; then
  TS3ARGS="$TS3ARGS license_accepted=1"
  touch /data/.ts3server_license_accepted
fi

exec ./ts3server $TS3ARGS

