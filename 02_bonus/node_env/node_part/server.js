'use strict'

const express = require('express');
const cors = require('cors');
var MongoClient = require("mongodb").MongoClient;

// Constant
const PORT = 8080;
const HOST = '0.0.0.0';


// App
const app = express();
app.use(cors());
app.get('/', (req, res) => {
	MongoClient.connect("mongodb://192.168.99.100/test_db", function(error, db) {
		if (error) return res.send(400);
		console.log("MongoDB connected to 'test_db' with success");

		db.createCollection("personnages", function(error, result) {
			if (error) return res.send(500);
			console.log("Collection created!");
		});
		var myobj = {name: "Hi there", address: "Highway 37"};
		db.collection("personnages").insertOne(myobj, function(error, result) {
			if (error) return res.send(500);
			console.log("1 document inserted");
		});
		db.collection("personnages").find().toArray(function(error, results) {
			if (error) return res.send(404);
			console.log(results);
			res.send(results);
		});
	});
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
