import React, { Component } from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			name: ''
		}
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		axios.get('http://192.168.99.100:8080/', {
					crossdomain: true,
					headers: {
						'Access-Control-Allow-Origin': '*'
					}
				})
			.then(response => {
				console.log(response.data);
				this.setState({name: response.data[0].name});
			});
	}

	render() {
		return (
				<div className="App">
					<div className="App-header">
						<img src={logo} className="App-logo" alt="logo" />
						<h2>Welcome to React</h2>
					</div>
					<p className="App-intro">
						To get started, edit <code>src/App.js</code> and save to reload.
					</p>
					<div className='button_container'>
						<button className='button' onClick={this.handleClick}>Click Me</button>
						<p>{this.state.name}</p>
					</div>
				</div>
			   );
	}
}

export default App;
